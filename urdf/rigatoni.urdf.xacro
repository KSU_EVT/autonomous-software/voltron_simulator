<robot name="rigatoni" xmlns:xacro="http://www.ros.org/wiki/xacro">

    <xacro:property name="use_gpu" value="false"/>

    <xacro:property name="filename" value="$(find voltron_simulator)/config/evgp_car_params.yaml"/>
    <xacro:property name="props" value="${load_yaml(filename)}"/>
    <xacro:property name="body_inertia" value="${props['body_inertia']}"/>
    <xacro:arg name="gpu" default="false"/>
    <xacro:property name="gpu" value="$(arg gpu)" />
    <xacro:arg name="organize_cloud" default="false"/>
    <xacro:property name="organize_cloud" value="$(arg organize_cloud)" />

    <!-- <xacro:include filename="$(find velodyne_description)/urdf/VLP-16.urdf.xacro"/> -->

    <!-- ************************** Links *************************** -->
    <xacro:macro name="null_inertia">
        <inertial>
            <mass value="0.0001"/>
            <inertia ixx="0.0001" ixy="0" ixz="0" iyy="0.0001" iyz="0" izz="0.0001"/>
        </inertial>
    </xacro:macro>

    <xacro:macro name="mesh_geometry">
        <geometry>
            <mesh filename="file://$(find voltron_simulator)/meshes/rigatoni.stl" scale="0.01 0.01 0.01"/>
        </geometry>
    </xacro:macro>

    <xacro:macro name="set_link_color" params="link color">
        <gazebo reference="${link}">
            <material>Gazebo/${color}</material>
        </gazebo>
    </xacro:macro>

    <xacro:macro name="evgp_body" params="collision_bitmask color">

        <!-- ************************** Links *************************** -->

        <link name="base_footprint">
            <!-- <xacro:null_inertia/> -->
        </link>

        <link name="body_visual_origin">
            <!-- <xacro:null_inertia/> -->
            <visual>
                <xacro:mesh_geometry/>
            </visual>
<!--            <collision>-->
<!--                <xacro:mesh_geometry/>-->
<!--            </collision>-->
        </link>
        <xacro:set_link_color link="body_visual_origin" color="${color}"/>
        <gazebo reference="body_visual_origin">
            <collision>
                <surface>
                    <contact>
                        <collide_bitmask>${collision_bitmask}</collide_bitmask>
                    </contact>
                </surface>
            </collision>
        </gazebo>

        <link name="body_inertial_center">
            <inertial>
                <mass value="${props['body_mass']}"/>
                <inertia ixx="${body_inertia[0]}" ixy="${body_inertia[1]}" ixz="${body_inertia[2]}"
                         iyy="${body_inertia[3]}" iyz="${body_inertia[4]}" izz="${body_inertia[5]}"/>
            </inertial>
        </link>
        <gazebo reference="body_inertial_center"/>


        <!-- ************************** Joints *************************** -->

        <joint name="base_footprint_to_mesh_origin" type="fixed">
            <parent link="base_footprint"/>
            <child link="body_visual_origin"/>
            <origin xyz="0.53 0 -0.05" rpy="${pi/2} 0 ${pi/2}"/>
        </joint>

        <joint name="base_footprint_to_body_inertial_center" type="fixed">
            <parent link="base_footprint"/>
            <child link="body_inertial_center"/>
            <origin xyz="${' '.join(str(x) for x in props['body_com'])}"/>
        </joint>

    </xacro:macro>
    <xacro:evgp_body collision_bitmask="0x1" color="Grey"/>

    <xacro:macro name="wheel_link" params="name radius width">
        <link name="${name}">
            <inertial>
                <mass value="0.01"/>
                <inertia ixx="0.001" ixy="0" ixz="0" iyy="0.001" iyz="0.0"
                         izz="${0.5 * props['wheel_mass'] * pow(radius, 2)}"/>
            </inertial>
            <visual>
                <geometry>
                    <cylinder radius="${radius}" length="${width}"/>
                </geometry>
            </visual>
            <collision>
                <geometry>
                    <cylinder radius="${radius}" length="${width}"/>
                </geometry>
            </collision>
        </link>
        <gazebo reference="${name}">
            <implicitSpringDamper>true</implicitSpringDamper>
            <mu1>0.7</mu1>
            <mu2>0.7</mu2>
            <kp>1000000</kp>
            <minDepth>0.001</minDepth>
            <kd>1</kd>
            <collision>
                <surface>
                    <contact>
                        <collide_bitmask>0x1</collide_bitmask>
                    </contact>
                </surface>
            </collision>
        </gazebo>
        <xacro:set_link_color link="${name}" color="Black"/>
    </xacro:macro>
    <xacro:wheel_link name="wheel_BL" radius="${props['wheel_radius_back']}" width="${props['wheel_width_back']}"/>
    <xacro:wheel_link name="wheel_BR" radius="${props['wheel_radius_back']}" width="${props['wheel_width_back']}"/>
    <xacro:wheel_link name="wheel_FL" radius="${props['wheel_radius_front']}" width="${props['wheel_width_front']}"/>
    <xacro:wheel_link name="wheel_FR" radius="${props['wheel_radius_front']}" width="${props['wheel_width_front']}"/>

    <link name="steering_link_left">
        <xacro:null_inertia/>
    </link>
    <link name="steering_link_right">
        <xacro:null_inertia/>
    </link>


    <!-- ************************** Joints *************************** -->

    <xacro:property name="joint_flop_both" value="0.01"/>
    <xacro:macro name="back_wheel_joint" params="dest_link y">
        <joint name="base_footprint_to_${dest_link}" type="continuous">
            <parent link="base_footprint"/>
            <child link="${dest_link}"/>
            <origin xyz="0.01 ${y} ${props['wheel_radius_back'] / 2}" rpy="-${pi/2} 0 0"/>
            <axis xyz="0 0 1"/>
            <dynamics damping="0.1" friction="0.03"/>
            <limit effort="10.0" velocity="3000.0" lower="-200.0" upper="2000.0" />
        </joint>
        <gazebo reference="base_footprint_to_${dest_link}"/>
    </xacro:macro>
    <xacro:back_wheel_joint dest_link="wheel_BL" y="${props['track'] / 2}"/>
    <xacro:back_wheel_joint dest_link="wheel_BR" y="${-props['track'] / 2}"/>

    <xacro:property name="joint_flop_front" value="0.007"/>
    <xacro:macro name="steering_joints" params="steering_link wheel_link y">
        <joint name="base_footprint_to_${steering_link}" type="revolute">
            <parent link="base_footprint"/>
            <child link="${steering_link}"/>
            <origin xyz="${props['wheelbase']} ${y} ${props['wheel_radius_front'] / 2}" rpy="0 0 0"/>
            <axis xyz="0 0 1"/>
            <dynamics damping="0.01" friction="0.003"/>
            <limit lower="${-props['max_steering']}" upper="${props['max_steering']}" effort="10000.0"
                   velocity="${props['steering_speed']}"/>
            <safety_controller k_position="100"
                    k_velocity="10"
                    soft_lower_limit="0.1"
                    soft_upper_limit="0.9" />
        </joint>
        <gazebo reference="base_footprint_to_${steering_link}"/>

        <joint name="${steering_link}_to_${wheel_link}" type="continuous">
            <origin xyz="0 0 ${-joint_flop_both - joint_flop_front}" rpy="${pi/2} 0 0"/>
            <parent link="${steering_link}"/>
            <child link="${wheel_link}"/>
            <axis xyz="0 0 1"/>
            <dynamics damping="0.1" friction="0.03"/>
            <limit effort="4.0" velocity="3000.0" lower="-200.0" upper="2000.0" />
        </joint>
        <gazebo reference="${steering_link}_to_${wheel_link}"/>
    </xacro:macro>
    <xacro:steering_joints steering_link="steering_link_left" wheel_link="wheel_FL" y="${props['track'] / 2}"/>
    <xacro:steering_joints steering_link="steering_link_right" wheel_link="wheel_FR" y="${-props['track'] / 2}"/>


    <xacro:macro name="set_base_footprint_to_link" params="link_name xyz rpy">
        <joint name="base_footprint_to_${link_name}" type="fixed">
            <parent link="base_footprint"/>
            <child link="${link_name}"/>
            <origin xyz="${xyz}" rpy="${rpy}"/>
        </joint>
    </xacro:macro>



    <!-- ******************* Sensors *********************** -->

    <!-- Velodyne Puck VLP-16 -->
    <!-- <xacro:VLP-16 parent="base_footprint" name="lidar" topic="/velodyne_points" hz="10" samples="500" gpu="true">
        <origin xyz="1.175 0 0.175" rpy="0 0 0" />
    </xacro:VLP-16> -->

    <!-- FLIR Blackfly + wide-angle lens -->


    <!-- GPS -->

    <xacro:include filename="$(find velodyne_description)/urdf/VLP-16.urdf.xacro"/>
    <xacro:VLP-16 parent="base_footprint" name="velodyne" topic="/velodyne_points" organize_cloud="${organize_cloud}" hz="10" samples="440" gpu="${gpu}">
        <origin xyz="1 0 0.4" rpy="0 0 0" />
    </xacro:VLP-16>

    
    <link name="gps_link"/>
    <gazebo reference="gps_link">
        <xacro:null_inertia/>
        <visual>
            <geometry>
                <box size="0.05 0.05 0.05"/>
            </geometry>
        </visual>
        <sensor name="my_gps" type="gps">
          <always_on>true</always_on>
          <update_rate>10</update_rate>
          <gps>
            <position_sensing>
              <horizontal>
                <noise type="gaussian">
                  <mean>0.0</mean>
                  <stddev>2e-4</stddev>
                </noise>
              </horizontal>
              <vertical>
                <noise type="gaussian">
                  <mean>0.0</mean>
                  <stddev>2e-4</stddev>
                </noise>
              </vertical>
            </position_sensing>
          </gps>
            <plugin name="gps_controller" filename="libgazebo_ros_gps_sensor.so">
                <ros>
                <namespace>/gps</namespace>
                <argument>~/out:=data</argument>
                </ros>
            </plugin>
         </sensor>
    </gazebo>
    
    <xacro:set_link_color link="gps_link" color="Gold"/>
    

    <link name="imu_link"/>
    <gazebo reference="imu_link">
        <xacro:null_inertia/>
        <visual>
            <geometry>
                <box size="0.05 0.05 0.05"/>
            </geometry>
        </visual>
      <sensor name="my_imu" type="imu">
        <always_on>true</always_on>
        <!-- Publish at 30 hz -->
        <update_rate>30</update_rate>
        <plugin name="my_imu_plugin" filename="libgazebo_ros_imu_sensor.so">
          <ros>
            <!-- Will publish to /imu/data -->
            <namespace>/imu</namespace>
            <argument>~/out:=data</argument>
          </ros>
          <!-- frame_name ommited, will be "my_link" -->
        </plugin>
      </sensor>
    </gazebo>

    <gazebo>
    <plugin filename="libgazebo_ros_p3d.so" name="ground_truth_tracker">
      <ros>
        <!-- Add namespace and remap the default topic -->
        <namespace>/demo</namespace>
        <argument>odom:=p3d_demo</argument>
      </ros>
  
      <update_rate>100.0</update_rate>
      <body_name>base_footprint</body_name>
      <gaussian_noise>0.0</gaussian_noise>
      <frame_name>map</frame_name>
      <xyz_offset>0 0 0</xyz_offset>
      <rpy_offset>0 0 0</rpy_offset>
    </plugin>
  </gazebo>
    
    <xacro:set_link_color link="imu_link" color="Gold"/>

    <xacro:set_base_footprint_to_link link_name="imu_link" xyz="0 0 0.21" rpy="0 0 0"/>
    <xacro:set_base_footprint_to_link link_name="gps_link" xyz="0.55 0.39 1.22" rpy="0 0 0"/>

    <!-- ****************** Actuators ********************** -->

    <gazebo>
        <plugin name="gazebo_ros2_control" filename="libgazebo_ros2_control.so">
            <robotNamespace>/</robotNamespace>
            <parameters>$(find voltron_simulator)/config/evgp_car_control.yaml</parameters>
        </plugin>
    </gazebo>
    <ros2_control name="GazeboSystem" type="system">
    <hardware>
      <plugin>gazebo_ros2_control/GazeboSystem</plugin>
    </hardware>
    <joint name="base_footprint_to_wheel_BL">
      <command_interface name="effort">
        <param name="min">-1</param>
        <param name="max">1</param>
      </command_interface>
      <state_interface name="position"/>
    </joint>
    <joint name="base_footprint_to_wheel_BR">
      <command_interface name="effort">
        <param name="min">-1</param>
        <param name="max">1</param>
      </command_interface>
      <state_interface name="position"/>
    </joint>
    <joint name="base_footprint_to_steering_link_left">
      <command_interface name="position">
        <param name="min">-0.3</param>
        <param name="max">0.3</param>
      </command_interface>
      <state_interface name="position"/>
    </joint>
    <joint name="base_footprint_to_steering_link_right">
      <command_interface name="position">
        <param name="min">-0.3</param>
        <param name="max">0.3</param>
      </command_interface>
      <state_interface name="position"/>
    </joint>

  </ros2_control>

</robot>
