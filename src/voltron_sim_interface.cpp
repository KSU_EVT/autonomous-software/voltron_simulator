#include "voltron_sim_interface.hpp"

VoltronSimInterface::VoltronSimInterface(const rclcpp::NodeOptions & options)
: rclcpp::Node("voltron_sim_interface", options)
{
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    publisher_ = this->create_publisher<trajectory_msgs::msg::JointTrajectory>("joint_trajectory_controller/joint_trajectory", 10);
    thr_publisher_ = this->create_publisher<std_msgs::msg::Float64MultiArray>("effort_controllers/commands", 10);
    joy_subscriber_ = this->create_subscription<sensor_msgs::msg::Joy>("joy", 10, std::bind(&VoltronSimInterface::joy_callback, this, _1));
    drive_subscriber_ = this->create_subscription<ackermann_msgs::msg::AckermannDriveStamped>(
        "/control/pure_pursuit/ackermann_cmd",
        10,
        std::bind(&VoltronSimInterface::drive_callback, this, _1));
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
}

void VoltronSimInterface::drive_callback(const ackermann_msgs::msg::AckermannDriveStamped& msg) const {
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    trajectory_msgs::msg::JointTrajectory message;
    std_msgs::msg::Float64MultiArray thr_msg;
    // thr_msg.header.stamp = rclcpp::Time(0);
    message.header.stamp = rclcpp::Time(0);
    trajectory_msgs::msg::JointTrajectoryPoint point;
    double s_val = msg.drive.steering_angle * (-2.0 / 3.14);
    point.positions.push_back(s_val);
    point.positions.push_back(s_val);
    
    // steering
    // geometry
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    // message.points[0].positions[0] = (float)rcvdmsg.axes[2];
    // message.points[0].positions[1] = (float)rcvdmsg.axes[2];
    message.points.push_back(point);
    message.joint_names.push_back("base_footprint_to_steering_link_right");
    message.joint_names.push_back("base_footprint_to_steering_link_left");

    // FIXME msg.drive.speed is *velocity* not *throttle*
    thr_msg.data.push_back(msg.drive.speed * 5);
    thr_msg.data.push_back(msg.drive.speed * 25);
    // message.points[0].positions[1]
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    // message.data.
    // rcvdmsg.data.axes[1] = throttle
    // rcvdmsg.data.axes[2] = steering (-1 right turn, 1 left turn)
    // message.data = "Hello, world! " + std::to_string(count_++);
    // RCLCPP_INFO(this->get_logger(), "Publishing: %f", rcvdmsg.axes[2]);
    publisher_->publish(message);
    thr_publisher_->publish(thr_msg);
}

void VoltronSimInterface::joy_callback(const sensor_msgs::msg::Joy & rcvdmsg) const
{
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    trajectory_msgs::msg::JointTrajectory message;
    std_msgs::msg::Float64MultiArray thr_msg;
    // thr_msg.header.stamp = rclcpp::Time(0);
    message.header.stamp = rclcpp::Time(0);
    trajectory_msgs::msg::JointTrajectoryPoint point;
    point.positions.push_back(rcvdmsg.axes[2]);
    point.positions.push_back(rcvdmsg.axes[2]);
    thr_msg.data.push_back(rcvdmsg.axes[1] * 5);
    thr_msg.data.push_back(rcvdmsg.axes[1] * 25);
    
    // steering
    // geometry
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    // message.points[0].positions[0] = (float)rcvdmsg.axes[2];
    // message.points[0].positions[1] = (float)rcvdmsg.axes[2];
    message.points.push_back(point);
    message.joint_names.push_back("base_footprint_to_steering_link_right");
    message.joint_names.push_back("base_footprint_to_steering_link_left");
    // message.points[0].positions[1]
    // RCLCPP_INFO(this->get_logger(), "asdfasdfasdfasfd");
    // message.data.
    // rcvdmsg.data.axes[1] = throttle
    // rcvdmsg.data.axes[2] = steering (-1 right turn, 1 left turn)
    // message.data = "Hello, world! " + std::to_string(count_++);
    // RCLCPP_INFO(this->get_logger(), "Publishing: %f", rcvdmsg.axes[2]);
    publisher_->publish(message);
    thr_publisher_->publish(thr_msg);
}