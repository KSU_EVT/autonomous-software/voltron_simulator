#include "voltron_sim_interface.hpp"

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<VoltronSimInterface>(rclcpp::NodeOptions()));
  rclcpp::shutdown();
  return 0;
}
